import { Box, Container, Grid, Typography } from '@mui/material';
import { readFile } from 'fs/promises';
import Project, { ProjectModel } from '../src/Project';

interface Props {
  projects: ProjectModel[];
}

export default function Projects({ projects }: Props) {
  return (
    <Container maxWidth="lg">
      <Typography variant="h2" className="centered">
        My projects
      </Typography>
      <Typography variant="body1" className="centered">
        Here are some of the projects I've worked on.
      </Typography>
      <Grid container spacing={2} sx={{ mt: 1 }}>
        {projects.map((project, id) => (
          <Grid item lg={3} md={4} xs={6} key={id}>
            <Project project={project} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}

export async function getStaticProps() {
  const content = await readFile('./assets/projects.json', 'utf8');

  const parsed = JSON.parse(content);

  return {
    props: {
      projects: parsed,
    },
  };
}
