import { PlayArrow, PlayArrowOutlined } from '@mui/icons-material';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  Stack,
  Typography,
} from '@mui/material';
import Link from 'next/link';
import React from 'react';

export interface ProjectModel {
  name: string;
  description: string;
  url: string;
  image?: string;
  tags?: string[];
  try?: string;
}

interface ProjectProps {
  project: ProjectModel;
}

const colors: { [index: string]: string } = {
  typescript: '#2d79c7',
  react: '#448a9e',
  rust: '#f47a00',
  python: '#968028',
  arduino: '#2f6f9f',
  'c++': '#004283',
  'group project': '#646464',
  'go': '#00aed8',
};

function getColor(tag: string) {
  if (tag in colors) {
    return colors[tag];
  }

  return '#646464';
}

export default function Project({ project }: ProjectProps) {
  return (
    <Card sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
      <CardContent sx={{ flexGrow: 1 }}>
        <Stack
          direction="row"
          spacing={1}
          sx={{ height: '2rem', textAlign: 'right', color: 'red' }}
        >
          {project.tags &&
            project.tags.map((tag, id) => {
              const color = getColor(tag);
              return (
                <Chip
                  key={id}
                  label={tag}
                  size="small"
                  variant="filled"
                  sx={{ backgroundColor: color, color: '' }}
                />
              );
            })}
        </Stack>
        <Typography variant="h5">{project.name}</Typography>
        <Typography variant="body1">{project.description}</Typography>
      </CardContent>
      <CardActions sx={{ display: 'flex' }}>
        <Link href={project.url} passHref>
          <Button size="small" color="primary">
            Project page
          </Button>
        </Link>
        <Box flexGrow={1} />
        {project.try && (
          <Link href={project.try} passHref>
            <Button
              endIcon={<PlayArrowOutlined />}
              size="small"
              color="primary"
              variant="outlined"
            >
              Try it
            </Button>
          </Link>
        )}
      </CardActions>
    </Card>
  );
}
